package com.translate.browser;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TranslateBrowserApplication {

	public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(TranslateBrowserApplication.class).headless(false).run(args);
        Application application = context.getBean(Application.class);
		application.start();
	}
}
