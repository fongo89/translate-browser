package com.translate.browser;

import chrriis.dj.nativeswing.NativeSwing;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import com.melloware.jintellitype.JIntellitype;
import com.translate.browser.configuraitons.FunctionKey;
import com.translate.browser.presenters.MainPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@Component("application")
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    @Autowired
    private MainPresenter presenter;

    @Value("${hotkey}")
    private String hotkey;

    public Application() {
        NativeSwing.initialize();
        NativeInterface.open();
    }

    public void start() {
        registerGlobalHotkeys();
        addSystemTrayIcon();
        NativeInterface.runEventPump();
    }

    private void addSystemTrayIcon() {
        Image image = Toolkit.getDefaultToolkit().getImage("src/main/resources/dictionary-icon.png");
        TrayIcon trayIcon = new TrayIcon(image);
        trayIcon.setImageAutoSize(true);
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                presenter.onTrayIconClick();
            }
        });

        try {
            SystemTray.getSystemTray().add(trayIcon);
        } catch (AWTException e) {
            LOG.error("Error while trying to add tray icon.", e);
        }
    }

    private void registerGlobalHotkeys() {
        FunctionKey hotkey = FunctionKey.valueOf(this.hotkey.toUpperCase());
        JIntellitype.getInstance().registerHotKey(1, 0, hotkey.getKeycode());
        JIntellitype.getInstance().addHotKeyListener(id -> {
            if (id == 1) {
                try {
                    LOG.info("Captured hotkey id = " + id);
                    presenter.onSearchHotkeyPressed();
                } catch (Exception e) {
                    LOG.error("Error while trying to capture text", e);
                }
            }
        });
    }




}
