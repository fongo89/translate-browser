package com.translate.browser.presenters;

public interface MainPresenter {
    void onTrayIconClick();

    void onSearchHotkeyPressed();
}
