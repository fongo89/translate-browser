package com.translate.browser.presenters;

import com.sun.jna.platform.win32.User32;
import com.translate.browser.frames.TranslatorFrame;
import com.translate.browser.services.CopyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component
public class MainPresenterImpl implements MainPresenter {

    @Autowired
    private TranslatorFrame frame;

    @Autowired
    private CopyService copyService;

    @Value("${uri.protocol}")
    private String protocol;

    @Value("${uri.host}")
    private String host;

    @Value("${uri.langFrom}")
    private String langFrom;

    @Value("${uri.langTo}")
    private String langTo;

    @Override
    public void onTrayIconClick() {
        int currentState = frame.getState();
        if(currentState == Frame.ICONIFIED)
            frame.setState(Frame.NORMAL);
        else if(currentState == Frame.NORMAL)
            frame.setState(Frame.ICONIFIED);
    }

    @Override
    public void onSearchHotkeyPressed() {
        executeTranslation();
    }

    private void bringWindowToFront() {
        frame.toFront();
        frame.repaint();
    }

    public void executeTranslation() {
        String capturedText = copyService.getSelectedText(User32.INSTANCE, CopyService.CustomUser32.INSTANCE);
        final String uri = buildUri(capturedText);
        bringWindowToFront();
        SwingUtilities.invokeLater(() -> frame.getWebBrowser().navigate(uri));
    }

    private String buildUri(String capturedText) {
        return String.format("%s://%s/#%s/%s/%s",protocol, host, langFrom, langTo, capturedText);
    }



}
