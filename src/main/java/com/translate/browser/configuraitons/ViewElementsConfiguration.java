package com.translate.browser.configuraitons;

import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import com.translate.browser.frames.TranslatorFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class ViewElementsConfiguration {

    @Bean(name = "webBrowser")
    public JWebBrowser jWebBrowserBean() {
        return new JWebBrowser();
    }

    @Bean
    @DependsOn(value = {"application", "webBrowser"})
    public TranslatorFrame mainFrame(@Autowired JWebBrowser jWebBrowser) {
        return new TranslatorFrame(jWebBrowser);
    }

}
