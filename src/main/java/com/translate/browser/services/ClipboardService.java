package com.translate.browser.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;

@Component
public class ClipboardService implements ClipboardOwner {

    private static final Logger LOG = LoggerFactory.getLogger(ClipboardService.class);

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        LOG.info("Inside lostOwnership method.");
    }

    public String getClipboardText()  {
        String clipboardContent = "";
        try {
            clipboardContent = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException e) {
            LOG.error("Error while capturing system clipboard content", e);
        }
        return clipboardContent;
    }

}
