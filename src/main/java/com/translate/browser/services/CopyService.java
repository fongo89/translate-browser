package com.translate.browser.services;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.win32.StdCallLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CopyService {

    private static final Logger LOG = LoggerFactory.getLogger(CopyService.class);

    @Autowired
    private ClipboardService clipboardService;

    void controlC(CustomUser32 customUser32) {
        customUser32.keybd_event((byte) 0x11 /* VK_CONTROL*/, (byte) 0, 0, 0);
        customUser32.keybd_event((byte) 0x43 /* 'C' */, (byte) 0, 0, 0);
        customUser32.keybd_event((byte) 0x43 /* 'C' */, (byte) 0, 2 /* KEYEVENTF_KEYUP */, 0);
        customUser32.keybd_event((byte) 0x11 /* VK_CONTROL*/, (byte) 0, 2 /* KEYEVENTF_KEYUP */, 0);// 'Left Control Up
    }

    public String getSelectedText(User32 user32, CustomUser32 customUser32) {
        WinDef.HWND hwnd = customUser32.GetForegroundWindow();
        char[] windowText = new char[512];
        user32.GetWindowText(hwnd, windowText, 512);
        String windowTitle = Native.toString(windowText);
        LOG.info("Will take selected text from the following window: [" + windowTitle + "]");
        String before = clipboardService.getClipboardText();
        controlC(customUser32); // emulate Ctrl C
        try {
            Thread.sleep(700); // give it some time
        } catch (InterruptedException e) {
            LOG.error("Error while sleeping thread", e);
        }
        String text = clipboardService.getClipboardText();
        LOG.info("Currently in clipboard: " + text);
        return text;
    }

    public interface CustomUser32 extends StdCallLibrary {
        CustomUser32 INSTANCE = (CustomUser32) Native.loadLibrary("user32", CustomUser32.class);
        WinDef.HWND GetForegroundWindow();
        void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
    }

}
