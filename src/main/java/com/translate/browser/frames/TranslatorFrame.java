package com.translate.browser.frames;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TranslatorFrame extends JFrame {

    private final JWebBrowser webBrowser;

    public TranslatorFrame(JWebBrowser webBrowser) {
        super("Translate");
        this.webBrowser = webBrowser;
        initForm(webBrowser);
    }

    private void initForm(JWebBrowser webBrowser) {
        setContentPane(webBrowser);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(820, (int)screenSize.getHeight() - 40));
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
        Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
        setLocation((int) rect.getMaxX() - getWidth(), 0);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                NativeInterface.close();
                System.exit(0);
            }
        });
    }

    public JWebBrowser getWebBrowser() {
        return webBrowser;
    }
}
